package fr.uavignon.shuet.tp1.data;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnimalList animal = new AnimalList();

        final ListView MyListView = (ListView) findViewById(R.id.MyListView);
        String[] values = animal.getNameArray(); // on recupère la liste des animeaux pour afficher sous forme de liste


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, values);
        MyListView.setAdapter(adapter);

        MyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
                intent.putExtra("name", item);
                startActivity(intent);
            }
        });
    }
}
