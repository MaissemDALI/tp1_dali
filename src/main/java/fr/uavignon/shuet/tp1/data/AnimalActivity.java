package fr.uavignon.shuet.tp1.data;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    TextView NomAnimal;
    TextView esp;
    TextView prd;
    TextView poid_naiss;
    TextView poid_adulte;
    EditText stat;
    Button save;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        NomAnimal = findViewById(R.id.NomAnimal);
        ImageView image = findViewById(R.id.imageAnimal);
        esp = findViewById(R.id.esp);
        prd = findViewById(R.id.prd);
        poid_naiss = findViewById(R.id.poid_naiss);
        poid_adulte = findViewById(R.id.poid_adulte);
        stat = findViewById(R.id.stat);


        AnimalList animal = new AnimalList();
        String name = getIntent().getStringExtra("name");
        NomAnimal.setText(name);

        final Animal animall = AnimalList.getAnimal(name);

        String photo = animall.getImgFile();
        image.setImageResource(getResources().getIdentifier( photo,"drawable",getPackageName()));

        esp.setText(animall.getStrHightestLifespan());
        prd.setText(animall.getStrGestationPeriod());
        poid_naiss.setText(animall.getStrBirthWeight());
        poid_adulte.setText(animall.getStrAdultWeight());
        stat.setText(animall.getConservationStatus());


        save = findViewById(R.id.id_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            animall.setConservationStatus(stat.getText().toString());
            Toast.makeText(getApplication(),"sauvgarde réussie ! ", Toast.LENGTH_LONG).show();
    }
        });}}

