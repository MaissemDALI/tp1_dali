package fr.uavignon.shuet.tp1.data;


import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;

public class MainRecyclerView  extends AppCompatActivity {

    // recup tout les noms de la hashmap
    private static final String[] items = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycler_view);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.ListRecycler);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL)); //recycler les vues dans une vue verticale
        rv.setAdapter(new MyAdapter());
    }
    // permet de charger les données dans le recycler view
    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        @NonNull
        @Override
        //charger les noms et les images
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.ligne, parent, false)));
        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            holder.bindModel(items[position]);
        }

        @Override
        public int getItemCount() {
            return(items.length);
        }
    }
    // organiser les donnes dans chaque item
    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView TextView;
        

        public RowHolder(View row) {
            super(row);
            row.setOnClickListener(this);
            TextView = (TextView)row.findViewById(R.id.nomAnimal);
            imageView = (ImageView)row.findViewById(R.id.imgAnimal);
        }

        @Override
        public void onClick(View v) {
            final String animalText = TextView.getText().toString();

            Intent intent = new Intent(MainRecyclerView.this,AnimalActivity.class);
            intent.putExtra("name",animalText); //envoyer des var d'une activité a l'autre
            startActivity(intent);
        }
    // charger l'image grace au name qui est un str grace au getImgFile()
        void bindModel(String item) {
            TextView.setText(item);
            Animal animal = AnimalList.getAnimal(item);
            // charger l'image de l'animal dans l'imageView
            Resources res = getResources();
            String mDrawableName = animal.getImgFile();
            int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID );
            imageView.setImageDrawable(drawable );
        }
    }
}
